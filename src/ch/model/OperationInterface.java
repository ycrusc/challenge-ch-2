package ch.model;

import java.util.List;

interface OperationInterface {

    void htgMode(List<Integer> m);

    void htgMean(List<Integer> m);

    void htgMedian(List<Integer> m);

}

